import React from "react";
import "./App.scss";

interface AppProps {
  //
}

interface AppState {
  value: string;
  error: boolean;
}

class App extends React.Component<AppProps, AppState> {
  buttons = [
    "AC",
    "(",
    ")",
    "/",
    "7",
    "8",
    "9",
    "*",
    "4",
    "5",
    "6",
    "-",
    "1",
    "2",
    "3",
    "+",
    "0",
    ".",
    "=",
  ];

  private _numberRegex = new RegExp("^[0-9]$");

  constructor(props: any) {
    super(props);
    this.state = { value: "", error: false } as AppState;

    this.handleChange = this.handleChange.bind(this);
  }

  isNumber(char: string) {
    return !!this._numberRegex.test(char);
  }

  private _isBracket(char: string) {
    return char === "(" || char === ")";
  }

  private _isCharAllowed(char: string) {
    const regex = new RegExp("^[0-9*/+-.\\(\\)]$");
    return char !== "," && regex.test(char);
  }

  private _isSpecialCharAllowed(char: string) {
    return !(!this.state.value.length && !this.isNumber(char));
  }

  private _handleSpecialCharReplace(char: string, event?: any) {
    const currentValue = this.state.value;
    const lastChar = currentValue[currentValue.length - 1];

    if (
      !this.isNumber(lastChar) &&
      !this._isBracket(lastChar) &&
      !this._isBracket(char)
    ) {
      if (event) {
        event.preventDefault();
      }

      this.setState({ value: currentValue.replace(/.$/, char) });
      return;
    }

    if (!event) {
      this.setState({ value: currentValue + char });
    }
  }

  handleKeyPress(event: any) {
    this.setState({ error: false });

    if (event.key === "Enter") {
      this._submit();
      return;
    }

    if (
      (event.key === "0" && !this.state.value.length) ||
      !this._isCharAllowed(event.key) ||
      !this._isSpecialCharAllowed(event.key)
    ) {
      event.preventDefault();
      return;
    }

    if (this.state.value.length && !this.isNumber(event.key)) {
      this._handleSpecialCharReplace(event.key, event);
    }
  }

  handleChange(event: any) {
    this.setState({ value: event.target.value });
  }

  handleButtonClick(button: string) {
    this.setState({ error: false });

    switch (button) {
      case "AC":
        this._clean();
        break;
      case "=":
        this._submit();
        break;
      default:
        this._addCharToInput(button);
    }
  }

  private _clean() {
    this.setState({ error: false });
    this.setState({ value: "" });
  }

  private _addCharToInput(char: string) {
    if (char === "0" && !this.state.value.length) {
      return;
    }

    if (!this.isNumber(char)) {
      this._handleSpecialCharReplace(char);
      return;
    }

    this.setState({ value: this.state.value + char });
  }

  private _submit() {
    this.setState({ error: false });

    if (!this.state.value.length) {
      return;
    }

    const requestData = JSON.stringify({ expr: this.state.value });

    fetch("https://api.mathjs.org/v4/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: requestData,
    })
      .then((res) => res.json())
      .then((data) => {
        if (data && data.error) {
          this.setState({ error: true });
          return;
        }

        this.setState({ value: data && data.result ? data.result : "" });
      })
      .catch(console.log);
  }

  NumberList() {
    return this.buttons.map((button, index) => (
      <span
        className={
          "calculator-app__button " +
          (button === "0" ? "calculator-app__button--double " : "") +
          (this.isNumber(button) ? "calculator-app__button--number" : "")
        }
        key={index.toString()}
        onClick={() => this.handleButtonClick(button)}
      >
        {button}
      </span>
    ));
  }

  render() {
    return (
      <div className="calculator-app">
        <div className="calculator-app__holder">
          <input
            className="calculator-app__input"
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress.bind(this)}
          />
          {this.NumberList()}
          {this.state.error && (
            <div className="calculator-app__error">Neispravan unos.</div>
          )}
        </div>
      </div>
    );
  }
}

export default App;
